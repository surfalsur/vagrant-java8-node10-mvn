#!/usr/bin/env bash
 

# Install Java 8
sudo apt-get install openjdk-8-jdk openjdk-8-jdk-headless -y

export PATH="$PATH:/usr/lib/jvm/java-8-openjdk-amd64/bin"
echo 'export PATH="$PATH:/usr/lib/jvm/java-8-openjdk-amd64/bin"' | sudo tee -a /home/vagrant/.bashrc

export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"
echo 'export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64"' | sudo tee -a /home/vagrant/.bashrc

echo "Installed java 8 ok **************************************************************"
 
